import {Component, OnInit} from '@angular/core';
import {UserServiceService} from "../../_services/user-service.service";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  allUsers: any = [];
  visible: boolean = false;
  selectedUser: any = [];
  confirmModal?: NzModalRef; // For testing by now
  isDeleted: any = false;

  constructor(
    private userService: UserServiceService,
    private modal: NzModalService,
  ) {
  }

  async ngOnInit(): Promise<any> {
    this.allUsers = await this.userService.getUsers()
  }

  closeDetails(): void {
    this.visible = false;
  }

  viewUserDetails(id: string) {
    this.visible = true;
    this.allUsers.map((user: any) => {
      if (user._id === id) {
        this.selectedUser = user
      }
    })
  }

  deleteSpecificUser(id: string, name: string) {
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Do you Want to delete this user?',
      nzContent: `You're about to remove ${name} user`,
      nzOnOk: () => new Promise<void>((resolve, reject) => {
        this.isDeleted = this.userService.deleteUser(id);
        if (this.isDeleted) {
          this.ngOnInit();
          resolve();
        }
      }).catch(() => console.log('Oops errors!'))
    });
  }

}
