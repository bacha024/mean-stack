import {Injectable} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  isLoggedIn = false;

  // isLoggedIn = true;
  constructor(
    private cookieService: CookieService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  isAuthenticated() {
    return !!this.cookieService.get("token");
  }

  async login(email: string, password: string): Promise<any> {
    const user = {
      email: email,
      password: password
    }
    try {
      const response = await fetch(`${environment.backendURL}login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
      })

      const resToJson = await response.json();
      this.cookieService.set('token', resToJson.token);
      this.isLoggedIn = true;
      return resToJson;

    } catch (error) {
      return error;
    }
  }
  async logout(): Promise<any> {
    this.cookieService.delete('token', '/');
    const response = {
      'message' : 'Logging out..'
    }
    return response;
  }
}
