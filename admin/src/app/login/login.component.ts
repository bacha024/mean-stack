import {Component, OnInit} from '@angular/core';
import {AuthServiceService} from "../_services/auth-service.service";
import {User} from "../_models/user";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoading: boolean = false;
  user: any = [];
  userModel = new User('', '')
  passwordVisible = false;

  constructor(
    private cookieService: CookieService,
    private authentication: AuthServiceService,
    private router: Router
  ) {
  }

  ngOnInit(): void {

  }

  async loginUser(): Promise<any> {
    this.isLoading = true;
    this.user = await this.authentication.login(
      this.userModel.email,
      this.userModel.password
    );
    if(this.user && this.user.errors) {
      // Handle Errors here
    } else {
      this.router.navigateByUrl('/admin/dashboard' ).then(() => {
        window.location.reload();
      });

    }
  }

}
